package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
)

type logWriter struct {
}

func main() {
	resp, err := http.Get("http://google.com")
	if err != nil {
		fmt.Printf("Error:", err)
		os.Exit(1)
	}

	bs := make([]byte, 99999)
	count, _ := resp.Body.Read(bs)
	fmt.Println(count, string(bs))

	io.Copy(os.Stdout, resp.Body)
	fmt.Println("=======================================")
	lw := logWriter{}
	io.Copy(lw, resp.Body)
}

func (logWriter) Write(bs []byte) (int, error) {
	fmt.Println(string(bs))
	fmt.Println("--------------Bytes:", len(bs))
	return len(bs), nil
}
