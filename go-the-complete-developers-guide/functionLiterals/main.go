package main

import (
	"fmt"
	"net/http"
	"time"
)

func main() {
	links := []string{
		"http://google.com",
		"http://facebook.com",
		"http://stackoverflow.com",
		"http://golang.org",
		"http://amazon.com",
	}

	c := make(chan string)

	for _, link := range links {
		// Go routine create a new thread go routine and run tis function with it
		go checkLink(link, c)
	}

	for l := range c {
		// go checkLink(l, c)
		go func(link string) { // Function literal
			time.Sleep(5 * time.Second)
			checkLink(link, c)
		}(l)
	}
}

func checkLink(link string, c chan string) {
	_, err := http.Get(link)
	if err != nil {
		fmt.Println(link + "might be down!")
		c <- link
		return
	}
	fmt.Println(link + " is up!")
	c <- link
}
