package main

import "fmt"

func main() {
	var b byte = 65
	fmt.Printf("Valor decimal: %d\n", b)     // Valor decimal: 65
	fmt.Printf("Valor hexadecimal: %X\n", b) // Valor hexadecimal: 41
	fmt.Printf("Valor binario: %08b\n", b)   // Valor binario: 01000001
	fmt.Printf("Carácter ASCII: %c\n", b)    // Carácter ASCII: A
}
