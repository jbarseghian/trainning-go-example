package main

import (
	"fmt"
	"strings"
)

// go run main.go deck.go
func main() {
	cards := newDeck()

	hand, remainingDeck := deal(cards, 5)
	hand.print()
	fmt.Println("===========================")
	remainingDeck.print()
	cards.print()

	greeting := "Hi there!"
	fmt.Println([]byte(greeting))

	fmt.Println(cards.toString())
	s := "{jime}"
	s, _ = strings.CutPrefix(s, "{")
	s, _ = strings.CutSuffix(s, "}")
	fmt.Println(s)

	fmt.Println(cards.toString())

	cards.saveToFile("my_cards")
	fmt.Println("Read file!!!!!!")
	cards1 := newDeckFomFile("my_cards")
	cards1.print()

	fmt.Println("===========================")
	cards2 := newDeck()
	cards2.shuffle()
	cards2.print()
}

