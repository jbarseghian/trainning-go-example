package main

import (
	"fmt"
	"net/http"
	"time"
)

func main() {
	links := []string{
		"http://google.com",
		"http://facebook.com",
		"http://stackoverflow.com",
		"http://golang.org",
		"http://amazon.com",
	}

	c := make(chan string)

	for _, link := range links {
		// Go routine create a new thread go routine and run tis function with it
		go checkLink(link, c)
	}

	for l := range c {
		//fmt.Println(<-c) // recibir el valor del canal
		go checkLink(l, c)
	}
}

func checkLink(link string, c chan string) {
	time.Sleep(5 * time.Second)

	_, err := http.Get(link)
	if err != nil {
		fmt.Println(link + "might be down!")
		c <- link
		return
	}
	fmt.Println(link + " is up!")
	c <- link
}
