package main

import "fmt"

func main() {
	elements := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

	for _, element := range elements {
		if element%2 == 0 {
			fmt.Println(element, "even")
		} else {
			fmt.Println(element, "odd")

		}
	}
	fmt.Println(elements)
}
