package main

import "fmt"

type ContactInfo struct {
	email   string
	zipCode int
}

type Person struct {
	firstName string
	lastName  string
	contact   ContactInfo
}

func main() {
	p1 := Person{
		firstName: "Alex",
		lastName:  "Anderson",
		contact: ContactInfo{
			email:   "alex@gmail.com",
			zipCode: 100,
		},
	}
	p1.updateName("Alex1")
	p1.print()
}

func (p *Person) updateName(newFirstName string) {
	p.firstName = newFirstName
}

func (p Person) print() {
	fmt.Printf("%+v", p)
}
