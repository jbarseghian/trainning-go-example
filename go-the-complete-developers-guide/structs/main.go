package main

import "fmt"

type Person struct {
	fistName string
	lastname string
}

func main() {
	fmt.Println()
	p := Person{
		fistName: "Jimena",
		lastname: "Barseghian",
	}

	fmt.Println(p)
	var alex Person
	//Zero value of string is ""
	fmt.Println("Zero value of string is", alex)
	fmt.Printf("%+v", alex)

	alex.fistName = "Alex"
	alex.lastname = "Anderson"
	fmt.Printf("\n%+v", alex)
}
