package main

import (
	"fmt"
	"io"
	"log"
	"os"
)

func main() {
	fmt.Println(os.Args)
	source := os.Args[1]

	file, err := os.Open(source) // For read access.
	if err != nil {
		log.Fatal(err)
	}

	io.Copy(os.Stdout, file)
}
