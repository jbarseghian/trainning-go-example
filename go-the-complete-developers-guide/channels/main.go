package main

import (
	"fmt"
	"net/http"
)

func main() {
	links := []string{
		"http://google.com",
		"http://facebook.com",
		"http://stackoverflow.com",
		"http://golang.org",
		"http://amazon.com",
	}

	c := make(chan string)

	for _, link := range links {
		// Go routine create a new thread go routine and run tis function with it
		go checkLink(link, c)
	}
	/*	fmt.Println(<-c) //wait data // receiving data is a blocking
		fmt.Println(<-c) //wait data
		fmt.Println(<-c) //wait data
		fmt.Println(<-c) //wait data
		fmt.Println(<-c) //wait data*/
	// fmt.Println(<-c) //wait data block

	for i := 0; i < len(links); i++ {
		fmt.Println(<-c) // recibir el valor del canal
	}
}

func checkLink(link string, c chan string) {
	_, err := http.Get(link)
	if err != nil {
		fmt.Println(link + "might be down!")
		c <- "Might be down I think!"
		return
	}
	fmt.Println(link + " is up!")
	c <- "Yes is up!"
}
