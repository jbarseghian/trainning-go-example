package handlers

import (
	"apirest/models"
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

func GetUsers(rw http.ResponseWriter, r *http.Request) {
	if users, err := models.ListUsers(); err != nil {
		models.SendNotFound(rw)
	} else {
		models.SendData(rw, users)
	}
	//fmt.Fprintln(rw, "Lista todos los usuarios")
	/*rw.Header().Set("Content-Type", "application/json")
	//rw.Header().Set("Content-Type", "text/xml")
	db.Connect()
	users, _ := models.ListUsers()
	db.Close()
	// Transformo el objeto users en json
	output, _ := json.Marshal(users)
	// Transformo el objeto users en xml
	//output, _ := xml.Marshal(users)
	// Transformo el objeto users en yaml
	//output, _ := yaml.Marshal(users)

	fmt.Fprintln(rw, string(output))*/
}

func GetUser(rw http.ResponseWriter, r *http.Request) {
	if user, err := getUserByRequest(r); err != nil {
		models.SendNotFound(rw)
	} else {
		models.SendData(rw, user)
	}
	//fmt.Fprintln(rw, "Obtiene un usuario")
	/*rw.Header().Set("Content-Type", "application/json")

	//Obtener ID
	vars := mux.Vars(r)
	userId, _ := strconv.Atoi(vars["id"])
	db.Connect()
	users, _ := models.GetUser(userId)
	db.Close()

	// Transformo el objeto users en json
	output, _ := json.Marshal(users)
	fmt.Fprintln(rw, string(output))*/
}

func CreateUser(rw http.ResponseWriter, r *http.Request) {
	user := models.User{}
	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&user); err != nil {
		models.SendUnprocessableEntity(rw)
	} else {
		user.Save()
		models.SendData(rw, user)
	}
	//fmt.Fprintln(rw, "Crea un usuario")

	/*rw.Header().Set("Content-Type", "application/json")

	//Obtener registro
	user := models.User{}
	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&user); err != nil {
		fmt.Fprintln(rw, http.StatusUnprocessableEntity)
	} else {
		db.Connect()
		user.Save()
		db.Close()
	}

	// Transformo el objeto users en json
	output, _ := json.Marshal(user)
	fmt.Fprintln(rw, string(output))*/
}

func UpdateUser(rw http.ResponseWriter, r *http.Request) {
	var userId int64

	if user, err := getUserByRequest(r); err != nil {
		models.SendNotFound(rw)
	} else {
		userId = user.Id
	}

	user := models.User{}
	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&user); err != nil {
		models.SendUnprocessableEntity(rw)
	} else {
		user.Id = userId
		user.Save()
		models.SendData(rw, user)
	}

	//fmt.Fprintln(rw, "Actualiza un usuario")

	/*rw.Header().Set("Content-Type", "application/json")

	//Obtener registro
	user := models.User{}
	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&user); err != nil {
		fmt.Fprintln(rw, http.StatusUnprocessableEntity)
	} else {
		db.Connect()
		user.Save()
		db.Close()
	}

	// Transformo el objeto users en json
	output, _ := json.Marshal(user)
	fmt.Fprintln(rw, string(output))*/
}

func DeleteUser(rw http.ResponseWriter, r *http.Request) {
	if user, err := getUserByRequest(r); err != nil {
		models.SendNotFound(rw)
	} else {
		user.Delete()
		models.SendData(rw, user)
	}
	//fmt.Fprintln(rw, "Borra un usuario")

	/*	rw.Header().Set("Content-Type", "application/json")

		//Obtener ID
		vars := mux.Vars(r)
		userId, _ := strconv.Atoi(vars["id"])
		db.Connect()
		user, _ := models.GetUser(userId)
		user.Delete()
		db.Close()

		// Transformo el objeto users en json
		output, _ := json.Marshal(user)
		fmt.Fprintln(rw, string(output))*/
}

func getUserByRequest(r *http.Request) (models.User, error) {
	//Obtener ID
	vars := mux.Vars(r)
	userId, _ := strconv.Atoi(vars["id"])
	if user, err := models.GetUser(userId); err != nil {
		return models.User{}, err
	} else {
		return *user, nil
	}

}
