package main

import (
	"apirest/handlers"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

func main() {
	// Rutas
	mux := mux.NewRouter()

	//Endpoints
	mux.HandleFunc("/api/user/", handlers.GetUsers).Methods("GET")
	mux.HandleFunc("/api/user/{id:[0-9]+}", handlers.GetUser).Methods("GET")
	mux.HandleFunc("/api/user/", handlers.CreateUser).Methods("POST")              // Registra
	mux.HandleFunc("/api/user/{id:[0-9]+}", handlers.UpdateUser).Methods("PUT")    // Editar
	mux.HandleFunc("/api/user/{id:[0-9]+}", handlers.DeleteUser).Methods("DELETE") // Eliminar

	//Crear un servidor
	log.Fatal(http.ListenAndServe(":3000", mux))

}
