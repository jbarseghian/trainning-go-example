package main

import (
	"fmt"
	"gomysql/db"
	"gomysql/models"
)

func main() {
	db.Connect()

	userI := models.CreateUser("roel", "roel123", "roel@gmail.com")
	fmt.Println(userI)

	user := models.GetUser(2)
	fmt.Println(user)
	user.Username = "juan"
	user.Password = "juan789"
	user.Email = "juan@gmail.com"
	user.Save()

	userD := models.GetUser(3)
	userD.Delete()

	users := models.ListUsers()
	fmt.Println(users)

	//db.TruncateTable("users")

	//fmt.Println(db.ExistsTable("users"))
	//db.TruncateTable("users")
	//db.CreateTable(models.UserSchema, "users")
	//db.Ping()
	db.Close()
}
