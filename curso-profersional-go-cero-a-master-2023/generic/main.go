package main

import (
	"fmt"
	"golang.org/x/exp/constraints"
)

type integer int

// Crear restricciones
type Numbers interface {
	~int | ~float64 | ~float32 | ~uint
}

func Sum[T constraints.Integer | constraints.Float](nums ...T) T {
	var total T
	for _, num := range nums {
		total += num
	}
	return total
}

func main() {
	var num1 integer = 100
	var num2 integer = 300
	fmt.Println(Sum(num1, num2))
	fmt.Println(Sum[uint](4, 5, 6))

}
