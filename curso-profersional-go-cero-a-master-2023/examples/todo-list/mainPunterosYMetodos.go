package main

import "fmt"

type Persona2 struct {
	// Campos
	nombre string
	edad   int
	correo string
}

// Método de esa structura Persona2, para ello debe tener un receptor la función
// En este caso el receptor es (p *Persona2)
// Go nos recomienda que el receptor sea un puntero para pasar ref de memoria
// Este método va a imprimir un mensaje
func (p *Persona2) diHola() {
	fmt.Println("Hola, mi nombre es", p.nombre)
}

// Métodos pueden tener parámetros igual que una función.
// La diferencia es que el método pertenece a una estructura y la funcion se llama in dependiente de una estructura

// Métodos de estructuras,se denominan funciones que tienen un receptor, que es un puntero o variable de la estructura
// Puntero: es una variable que almacena la dirección de la memoria de otra variable.
// Los punteros Se utilizan para referencias y acceder a la variable original atraves de la direccion de la memoria.
func main() {
	// Variable normal de tipo 10
	var x int = 10

	// Puntero a un tipo de dato int, mandamos la referecia de momoria de x a p
	var p *int = &x
	// Tanto p como x tienen la misma dirección de memoria
	fmt.Println(x)
	fmt.Println(p)

	// Imprimir ref memoria de x
	fmt.Println(&x)
	fmt.Println(p)

	// Imprimimos x
	fmt.Println("Inicio: ", x)
	// Enviamos la ref de memoria de x
	editar(&x)
	fmt.Println("Después editar(): ", x) // Imprime 20 ya que desde editar() trabajando con punteros

	// Cuando no usamos punteros
	// Imprimimos x
	x = 10
	fmt.Println("Inicio sin puntero: ", x)
	// Enviamos x
	editar2(x)
	fmt.Println("Después editar2() sin puntero: ", x) // Imprime 20 ya que desde editar() trabajando con punteros

	// Método de Persona2
	// Creo instancia de persona2
	p1 := Persona2{"Alex", 28, "alex@gmail.com"}
	// Ejecutar al método de Persona2 hago:
	p1.diHola()
}

// El puntero nos ayuda a pasar por refencia y poder cambiar el valor del dato de la variable

// Función editar que recibe un puntero, recibe puntero en variable x
func editar(x *int) {
	//Tenemos que poner *x ya que x es un puntero
	*x = 20
}

func editar2(x int) {
	//Tenemos que poner x ya que x no es un puntero
	x = 20
}
