package main

import (
	"fmt"
)

func main() {
	var a []int //slice o rebanada es dinámico
	a = append(a, 1)
	fmt.Println(a)

	diasSemana := []string{"Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"}

	diaRebanada := diasSemana[0:5]
	fmt.Println("diasSemana: ", diasSemana)
	fmt.Println("len(diasSemana): ", len(diasSemana))
	fmt.Println("diaRebanada := diasSemana[0:5] - 5 elementos: ", diaRebanada)

	// Capacidad inicial es 7
	// Al agregar Otro dia se amplia la capacidad a 15 y la cantidad de elementos a 8
	diaRebanada = append(diaRebanada, "Viernes", "Sábado", "Otro dia")

	fmt.Println("len(diaRebanada): ", len(diaRebanada))
	fmt.Println("cap(diaRebanada); ", cap(diaRebanada))
	fmt.Println("diaRebanada: ", diaRebanada)

	//Eliminar elemento Martes
	// Creo una rebanda con 0 cant 2, luego le agregamos desde el indice 3 (Miercoles) hasta el final
	diaRebanada = append(diaRebanada[:2], diaRebanada[3:]...)
	fmt.Println(" append(diaRebanada[:2], diaRebanada[3:]...)")
	fmt.Println("len(diaRebanada): ", len(diaRebanada))
	fmt.Println("cap(diaRebanada); ", cap(diaRebanada))
	fmt.Println("diaRebanada: ", diaRebanada)

	// Usando make, otra forma de crear un slice, map o chan
	fmt.Println("Usando make --------------------------")
	// Creo un slice, len de 5 y capacidad de 10
	nombres := make([]string, 5, 10)
	nombres[0] = "Alex"
	fmt.Println("nombres: ", nombres)
	fmt.Println("len(nombres): ", len(nombres))
	fmt.Println("cap(nombres): ", cap(nombres))

	rebanada1 := []int{1, 2, 3, 4, 5}
	rebanada2 := make([]int, 5)

	fmt.Println("rebanada1 := []int{1, 2, 3, 4, 5}", rebanada1)
	fmt.Println("rebanada2 := make([]int, 5)", rebanada2)

	fmt.Println("Usando copy --------------------------")
	// Copy la rebanada2 en rebanda2, imprime la cantidad de elementos
	fmt.Println(copy(rebanada2, rebanada1))

	fmt.Println("rebanada1: ", rebanada1)
	fmt.Println("rebanada2: ", rebanada2)

}
