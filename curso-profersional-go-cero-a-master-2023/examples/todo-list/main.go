package main

import "fmt"

func main() {
	var matriz = [5]int{10, 20, 30, 40, 50}

	fmt.Println(matriz)

	var matriz2 = [...]int{10, 20, 30, 40, 50}
	fmt.Println(matriz2)

	for i := 0; i < len(matriz2); i++ {
		fmt.Println(matriz2[i])
	}

	for i, v := range matriz2 {
		fmt.Printf("Indice %d, valor %d\n", i, v)
	}

	// matriz 3 x 3

	var matriz3 = [3][3]int{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}
	fmt.Println(matriz3)
}
