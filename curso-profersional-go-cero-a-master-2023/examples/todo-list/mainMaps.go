package main

import "fmt"

func main() {
	// Key: string y value string
	//rgb red green blue
	colors := map[string]string{
		"rojo":  "#FF0000",
		"verde": "#00FF00",
		"azul":  "#0000FF",
	}

	fmt.Println(colors)
	fmt.Println(colors["rojo"])

	// Agregar elemento
	colors["negro"] = "#000000"
	fmt.Println(colors)

	// ok es la verificación, si existe o no la clave
	valor, ok := colors["rojo"]
	fmt.Println(valor, ok)

	// Si no está retorna un string vacio
	if valor, ok = colors["verde"]; ok {
		fmt.Println("Si existe el key", valor)
	} else {
		fmt.Println("No existe el key")
	}

	// Eliminar un elemento, map y key
	delete(colors, "azul")
	fmt.Println(colors)

	// Iterar elementos del map
	for key, value := range colors {
		fmt.Printf("Clave: %s, Valor: %s\n", key, value)
	}
}
