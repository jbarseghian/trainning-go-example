package main

import "fmt"

// Tipo de dato Persona
// Una estructura nos permice almanceran tipos de datos
type Persona struct {
	// Campos
	nombre string
	edad   int
	correo string
}

func main() {
	// Variable o instancia de tipo persona
	var p Persona

	p.nombre = "Alex"
	p.edad = 28
	p.correo = "alex@gmail.com"

	fmt.Println(p)

	p1 := Persona{"Alex", 28, "alex@gmail.com"}
	p1.edad = 30
	fmt.Println(p1.nombre, p1)

	p2 := Persona{"Juan", 40, "juan@gmail.com"}
	fmt.Println(p2)

}
