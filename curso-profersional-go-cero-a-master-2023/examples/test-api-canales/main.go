package main

import (
	"fmt"
	"net/http"
	"time"
)

// Un canal es una estructura que permite enviar y recibir valores entre runtimes, actuando como un conducto a través del cual fluye la información
// Los canales sirven para comunicarse con los go runtimes
func main() {
	start := time.Now()

	apis := []string{
		"https://management.azure.com",
		"https://dev.azure.com",
		"https://api.github.com",
		"https://outlook.office.com/",
		"https://api.somewhereintheinternet.com/",
		"https://graph.microsoft.com",
	}

	fmt.Println(start)

	//declaro canal
	ch := make(chan string)

	for _, api := range apis {
		// Se ejecutan de manera muy rapida y no se ve, entonces pomemos un sleep
		// Se aplica concurrencia
		go checkApiCanal(api, ch)
	}

	// Mas rapido!
	for i := 0; i < len(apis); i++ {
		fmt.Println(<-ch)
	}

	elapsed := time.Since(start)
	fmt.Printf("¡Listo! ¡Tomó %v segundos!\n", elapsed.Seconds())
}

func main3() {
	start := time.Now()

	apis := []string{
		"https://management.azure.com",
		"https://dev.azure.com",
		"https://api.github.com",
		"https://outlook.office.com/",
		"https://api.somewhereintheinternet.com/",
		"https://graph.microsoft.com",
	}

	fmt.Println(start)

	//declaro canal
	ch := make(chan string)

	for _, api := range apis {
		// Se ejecutan de manera muy rapida y no se ve, entonces pomemos un sleep
		// Se aplica concurrencia
		go checkApiCanal(api, ch)
	}

	// imprimir lo que devuelve la funcion checkApiCanal
	// el primer mensaje
	fmt.Println(<-ch)

	fmt.Println(<-ch)
	fmt.Println(<-ch)
	fmt.Println(<-ch)
	fmt.Println(<-ch)
	fmt.Println(<-ch)

	// no detiene poeque espera
	fmt.Println(<-ch)
	fmt.Println(<-ch)

	elapsed := time.Since(start)
	fmt.Printf("¡Listo! ¡Tomó %v segundos!\n", elapsed.Seconds())
}

func main2() {

	// canal de tipo int
	canal := make(chan int)

	// enviar datos al canal
	canal <- 15
	// como recibe datos del canal
	valor := <-canal

	fmt.Println(valor)

}

// sin concurrencia
func main1() {
	start := time.Now()

	apis := []string{
		"https://management.azure.com",
		"https://dev.azure.com",
		"https://api.github.com",
		"https://outlook.office.com/",
		"https://api.somewhereintheinternet.com/",
		"https://graph.microsoft.com",
	}

	fmt.Println(start)
	for _, api := range apis {
		checkApi(api)
	}

	elapsed := time.Since(start)
	fmt.Printf("¡Listo! ¡Tomó %v segundos!\n", elapsed.Seconds())
}

func checkApi(api string) {
	if _, err := http.Get(api); err != nil {
		fmt.Printf("ERROR: ¡%s está caído! Message: %v\n", api, err)
		return
	}
	fmt.Printf("SUCCESS: ¡%s está en funcionamiento!\n", api)
}

func checkApiCanal(api string, ch chan string) {
	if _, err := http.Get(api); err != nil {
		ch <- fmt.Sprintf("ERROR: ¡%s está caído! Message: %v", api, err)
		return
	}
	ch <- fmt.Sprintf("SUCCESS: ¡%s está en funcionamiento!", api)
}
