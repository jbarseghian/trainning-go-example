package main

import (
	"fmt"
	"math/rand"
)

func main() {
	jugar()
}

func jugar() {
	numeroAleatorio := rand.Intn(100)

	var numIngresado int
	var intentos int

	const maxIntentos = 5
	for intentos < maxIntentos {
		intentos++
		fmt.Printf("Ingrese un número (intentos restantes: %d): ", maxIntentos-intentos+1)
		fmt.Scanln(&numIngresado)

		if numIngresado == numeroAleatorio {
			fmt.Println("Adivinaste el número!")
			jugarNuevamente()
			return
		} else if numIngresado < numeroAleatorio {
			fmt.Println("El número a adivinar es mayor")

		} else if numIngresado > numeroAleatorio {
			fmt.Println("El número a adivinar es menor")
		}
	}

	fmt.Println("Se acabaron los intentos. El número aleatoreo es: ", numeroAleatorio)
	jugarNuevamente()
}

func jugarNuevamente() {
	var eleccion string
	fmt.Print("¿Quieres jugar nuevamente? (s/n): ")
	fmt.Scanln(&eleccion)

	switch eleccion {
	case "s":
		jugar()
	case "n":
		fmt.Println("Gracias por jugar!")
	default:
		fmt.Println("Elección invalidada. Intentelo nuevamente.")
		jugarNuevamente()
	}
}
