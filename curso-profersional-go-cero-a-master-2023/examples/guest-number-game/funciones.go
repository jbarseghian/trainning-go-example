package main

import "fmt"

func main() {
	s := hello("jime")
	fmt.Println(s)
	t, t2 := calc(3, 2)
	fmt.Println(t, t2)
}

func hello(name string) string {
	return "Hola," + name
}

func calc(a, b int) (sum, mul int) {
	return a + b, a * b
}
