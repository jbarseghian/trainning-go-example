package main

import "fmt"

func main() {
	for i := 1; i <= 10; i++ {
		if i == 5 {
			continue
			// no imprime el 5 ya que hace continue y vuelve al bucle
		}
		fmt.Println(i)
	}
}
