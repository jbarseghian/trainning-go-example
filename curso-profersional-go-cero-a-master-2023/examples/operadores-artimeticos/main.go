package main

import (
	"fmt"
	"math"
)

func main() {
	c := math.Pow(2, 4)
	fmt.Printf("%.1f", c)
}
