package main

import "fmt"

// Clausura: funcion anonima que se define dentro de otra funcion

func incrementer() func() int {
	i := 0
	return func() int {
		i++
		return i
	}
}

func main1() {
	// define i = 0 e icrementa i en 1
	nextInt := incrementer()

	fmt.Println(nextInt())
	fmt.Println(nextInt())
	fmt.Println(nextInt())

}

func main() {
	x := 10

	// Creating a closure
	add := func(y int) int {
		return x + y
	}

	result := add(5)
	fmt.Println(result) // Output: 15
}
