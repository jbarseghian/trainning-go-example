package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
)

// https://www.udemy.com/course/curso-golang/learn/lecture/38174542#overview

// Vamos a guardar en el archivo formato json
type Contact struct {
	Name  string `json:"name"`
	Email string `json:"email"`
	Phone string `json:"phone"`
}

// GUardar contactos en un archivo json
// De objeto a arhivo encode
func saveContactsToFile(contacts []Contact) error {
	file, err := os.Create("contact.json")
	if err != nil {
		return err
	}

	defer file.Close()

	// Converto file
	// Serializacion de datos
	encoder := json.NewEncoder(file)
	err = encoder.Encode(contacts)
	if err != nil {
		return err
	}
	return nil
}

// cargar contactos desde un archivo json
// De json a objeto decode
func loadContactsFromFile(contacts *[]Contact) error {
	file, err := os.Open("contact.json")
	if err != nil {
		return err
	}
	defer file.Close()

	decoder := json.NewDecoder(file)
	err = decoder.Decode(&contacts) // lo mete en contacts
	if err != nil {
		return err
	}
	return nil
}

func main() {
	// Cargar contactos existentes desde el archivo
	var contacts []Contact
	err := loadContactsFromFile(&contacts)
	if err != nil {
		fmt.Println("Error al cargar los contactos:", err)
	} else {
		fmt.Println(contacts)
	}

	// Crear instancia de bufio
	reader := bufio.NewReader(os.Stdin)

	for {
		fmt.Print("==== GESTOR DE CONTACTOS ===\n",
			"1. Agregar un contacto\n",
			"2. Mostrar todos los contacto\n",
			"3. Salir\n",
			"Elige una opción: ")

		// Leer la opcion del usuario
		var opcion int
		_, err = fmt.Scanln(&opcion)
		if err != nil {
			fmt.Println("Error al leer la opción", err)
			return
		}
		fmt.Println(opcion)
		switch opcion {
		case 1:
			var c Contact
			fmt.Print("Nombre: ")
			c.Name, _ = reader.ReadString('\n')
			fmt.Print("Email: ")
			c.Email, _ = reader.ReadString('\n')
			fmt.Print("Teléfono: ")
			c.Phone, _ = reader.ReadString('\n')
			contacts = append(contacts, c)
			if err := saveContactsToFile(contacts); err != nil {
				fmt.Println("Error al guardar el contacto")
			}
		case 2:
			fmt.Println("=================================================")
			for index, contact := range contacts {
				fmt.Printf("%d. Nombre: %s Email: %s Teléfono: %s\n",
					index+1, contact.Name, contact.Email, contact.Phone)
			}
			fmt.Println("=================================================")
		case 3:
			// Salir del programa
			return
		default:
			fmt.Println("Opción no válida")
		}
	}

	fmt.Println(reader)
}

/*
func main() {
	contactos := []Contact{{Name: "Jime", Email: "jime@gmail.com", Phone: "123"}}
	saveContactsToFile(contactos)
	fmt.Println("Save:", contactos)
	var c []Contact
	loadContactsFromFile(&c)
	fmt.Println("Load:", c)
}*/
