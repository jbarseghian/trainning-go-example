package main

import (
	"fmt"
	"net/http"
	"time"
)

// para agergar concurrencia le agregamos una go rutine
func main() {
	start := time.Now()

	apis := []string{
		"https://management.azure.com",
		"https://dev.azure.com",
		"https://api.github.com",
		"https://outlook.office.com/",
		"https://api.somewhereintheinternet.com/",
		"https://graph.microsoft.com",
	}

	fmt.Println(start)
	for _, api := range apis {
		// Se ejecutan de manera muy rapida y no se ve, entonces pomemos un sleep
		// Se aplica concurrencia
		go checkApi(api)
	}

	time.Sleep(1 * time.Second) // tiempo de suspenso

	elapsed := time.Since(start)
	fmt.Printf("¡Listo! ¡Tomó %v segundos!\n", elapsed.Seconds())
}

// sin concurrencia
func main1() {
	start := time.Now()

	apis := []string{
		"https://management.azure.com",
		"https://dev.azure.com",
		"https://api.github.com",
		"https://outlook.office.com/",
		"https://api.somewhereintheinternet.com/",
		"https://graph.microsoft.com",
	}

	fmt.Println(start)
	for _, api := range apis {
		checkApi(api)
	}

	elapsed := time.Since(start)
	fmt.Printf("¡Listo! ¡Tomó %v segundos!\n", elapsed.Seconds())
}

func checkApi(api string) {
	if _, err := http.Get(api); err != nil {
		fmt.Printf("ERROR: ¡%s está caído! Message: %v\n", api, err)
		return
	}
	fmt.Printf("SUCCESS: ¡%s está en funcionamiento!\n", api)
}
