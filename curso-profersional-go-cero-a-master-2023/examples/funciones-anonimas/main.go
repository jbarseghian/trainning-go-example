package main

import "fmt"

func duplicar(n int) int {
	return n * 2
}

func triplicar(n int) int {
	return n * 3
}

func aplicar(f func(int) int, n int) int {
	return f(n)
}

func saludar(name string, f func(string)) {
	f(name)
}

// funcion anónima: función que no tiene nombre
func main() {
	func() {
		fmt.Println("Soy una función anónima!")
	}()

	saludo := func(name string) {
		fmt.Printf("Hola, %s!\n", name)
	}
	saludo("Jime")
	saludar("Jimenita", saludo)

	fmt.Println(aplicar(duplicar, 5))
	fmt.Println(aplicar(triplicar, 5))
}
