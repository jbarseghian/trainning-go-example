package main

import "fmt"

// funcion variádica
func suma(name string, nums ...int) int {
	var total int
	fmt.Printf("%T - %v - %s\n", nums, nums, name)
	for _, num := range nums {
		total += num
	}
	return total
}

// funcion variádica que recibe diferentes tipos de datos
func imprimirDatos(datos ...interface{}) {
	for _, dato := range datos {
		fmt.Printf("%T - %v\n", dato, dato)
	}
}

func main() {
	total := suma("sum1", 12, 45, 78, 56)
	fmt.Println(total)
	fmt.Println(suma("sum2", 10, 20, 30, 40, 50))
	imprimirDatos("Hola", 28, true, 3.14)
}
