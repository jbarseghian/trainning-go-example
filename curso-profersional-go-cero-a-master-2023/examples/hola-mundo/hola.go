package main

import (
	"fmt"
	"math"
	"strconv"

	"rsc.io/quote"
)

// Debo declarar e inicializar cuando es cte
const Pi = 3.14

const (
	Domingo = iota + 1
	Lunes
	Martes
	Miercoles
	Jueves
	Viernes
	Sabado
)

func main1() {
	fmt.Println("Hola mundo")
	fmt.Println(quote.Hello())
	fmt.Println(quote.Go())

	// Declaracion de variables
	//con var dentro y fuera de las funciones
	//var firstName,  lastName, age = "Alex", "Roel", 27

	//:= operador de declaracion y asignacion
	// solo dentro de functions
	firstName, lastName, age := "Alex", "Roel", 27
	fmt.Println(firstName, lastName, age)
	fmt.Println(Pi)
	fmt.Println(Domingo, Lunes, Martes, Miercoles, Jueves, Viernes, Sabado)

	var integer int8 = 127
	fmt.Println(integer)

	var integer1 int // depende del SO   y uint solo positivos frprnfr SO
	fmt.Println(integer1)

	fmt.Println(math.MinInt64, math.MaxInt64)

	fullName := "Alex Roel \t(alias \"roelcode\")\n"
	fmt.Println(fullName)

	var a byte = 'a'
	fmt.Println(a)

	s := "hola"
	fmt.Println(s[0]) // valor unicode de h

	var r rune = '♡'
	fmt.Println(r) // valor unicode del corazón

	// Valores predeterminados
	var (
		defaultInt    int
		defaultUint   uint
		defaultFloat  float32
		defaultBool   bool
		defaultString string
	)

	fmt.Println(defaultInt, defaultUint, defaultFloat, defaultBool, defaultString)

	// Conversion de tipos
	var integer16 int16 = 50
	var integer32 int32 = 100
	fmt.Println(int32(integer16) + integer32)

	//Conversiones mas complejas como string a int, necesitamos un paquete

	str := "100"
	i, _ := strconv.Atoi(str)
	fmt.Println(str, i)

	n := 42
	s = strconv.Itoa(n)
	fmt.Println(s + s)

	// El paquete fmt
	// https://www.udemy.com/course/curso-golang/learn/lecture/38174154#overview 21.
	name := "Alex"
	edad := 28
	fmt.Printf("Hola, me llamo %s y tengo %d años.\n", name, edad)

	// Formateamos y devuelve string
	greeting := fmt.Sprintf("Hola, me llamo %s y tengo %d años.", name, edad)
	fmt.Println(greeting)
}

// Entrada de datos
// https://www.udemy.com/course/curso-golang/learn/lecture/38174154#overview 21.
func main() {
	fmt.Println("Hola mundo")
	fmt.Println(quote.Go())

	var name string
	var age int

	fmt.Print("Ingrese su nombre: ")
	// Referencia de la memoria donde va a almacenar lo que va a escanear
	fmt.Scanln(&name)

	fmt.Print("Ingrese su edad: ")
	fmt.Scanln(&age)

	fmt.Printf("Hola, me llamo %s y tengo %d años.\n", name, age)

	fmt.Printf("El tipo de name es: %T\n", name)
	fmt.Printf("El tipo de age es: %T\n", age)
}

// %v	the value in a default format
// 	when printing structs, the plus flag (%+v) adds field names
// %#v	a Go-syntax representation of the value
// %T	a Go-syntax representation of the type of the value
// %%	a literal percent sign; consumes no value
