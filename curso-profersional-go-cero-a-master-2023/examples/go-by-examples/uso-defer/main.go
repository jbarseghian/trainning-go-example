package main

import (
	"fmt"
	"os"
)

func main() {
	file, err := os.Create("hola.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close() // Antes de terminar main

	_, err = file.Write([]byte("Hola Jime"))
	if err != nil {
		fmt.Println(err)
		return
	}
}

//func main() {
//	// Se agregan como pila
//	defer fmt.Println(3)
//	defer fmt.Println(2)
//	defer fmt.Println(1)
// Imprime 1, 2, 3
//}
