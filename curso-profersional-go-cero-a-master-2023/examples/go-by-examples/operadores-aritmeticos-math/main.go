package main

import (
	"fmt"
	"math"
)

func main() {

	a := 10
	b := 3
	fmt.Println(a / b)
	fmt.Println(a % b)

	b++
	fmt.Println(a + b)

	// https://pkg.go.dev/math
	fmt.Println(math.Pi)

	c := math.Pow(2, 3) // potencia
	fmt.Printf("%.1f\n", c)

	fmt.Println(math.Sqrt(64))
}
