package main

import "fmt"

func main() {
	// The code var matrix [5]int declares a variable named matrix as an array of integers with a fixed length of 5.
	// Each element in the array is of type int.
	var a [5]int
	a[0] = 10
	a[1] = 20

	fmt.Println(a)

	var b = [5]int{10, 20, 30, 40, 50}
	fmt.Println(b)

	// Cuando no sabemos la cantidad del array
	var c = [...]int{10, 20, 30, 40, 50}
	fmt.Println(c)
	//fmt.Println(c[9]) //invalid argument: index 9 out of bounds [0:5]

	fmt.Println("For i")
	for i := 0; i < len(c); i++ {
		fmt.Println(i, c[i])
	}
	fmt.Println("For range")
	for i, value := range c {
		fmt.Printf("Indice %d, Valor: %d\n", i, value)
	}

	fmt.Println("Matrix bidireccional")
	// filas columnas 3 x 3
	var matriz = [3][3]int{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}
	fmt.Println(matriz)
}
