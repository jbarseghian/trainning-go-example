module hello

go 1.20

replace gitlab.com/jbarseghian/greetings => ../greetings

require gitlab.com/jbarseghian/greetings v0.0.0-00010101000000-000000000000
