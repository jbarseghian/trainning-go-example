package main

import (
	"fmt"
	"gitlab.com/jbarseghian/greetings"
	"log"
)

// go mod edit -replace gitlab.com/jbarseghian/greetings=../greetings
func main() {
	log.SetPrefix("greetings: ")
	log.SetFlags(0) // No muestra fecha y hora

	message, err := greetings.Hello("Jime")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(message)

	messages, err := greetings.Hellos([]string{"Ambar", "Pepe"})
	fmt.Println(messages)
}
