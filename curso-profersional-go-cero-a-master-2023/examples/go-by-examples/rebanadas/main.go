package main

import "fmt"

//rebanadas o slices proporcionan manera de trabajar con un arreglo de manera dinámica
// Arreglo es fijo, el slice podes agregar y eleimnar elementos

func main() {
	var a []int // slice
	a = append(a, 1)
	fmt.Println(a)

	diasSemana := []string{"Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"}

	fmt.Println(diasSemana)
	diaRebanada := diasSemana[0:5] // Desde el inicio 2 elementos indice, cant
	fmt.Println(diaRebanada)

	diaRebanada = append(diaRebanada, "Viernes", "Sabado", "Otro")
	fmt.Println(diaRebanada)
	fmt.Println(len(diaRebanada)) //cantidad de elementos
	fmt.Println(cap(diaRebanada)) //capacidad se toma el origen de donde se creo que fue dia semana
	// Con esto probamos porque son dinamicas y crece la capacidad en base al origen primero 7 luego 14

	fmt.Println(diaRebanada)
	diaRebanada = append(diaRebanada[:2], diaRebanada[3:]...) // Eliminamos el Martes
	fmt.Println(diaRebanada)

	// Crear rebanada con make
	//log, capacidad inicial por ser slice
	nombres := make([]string, 5, 10)
	nombres[0] = "Alex"
	nombres = append(nombres, "Jime")
	fmt.Println(nombres)
	fmt.Println(len(nombres))
	fmt.Println(cap(nombres))

	rebanada1 := []int{1, 2, 3, 4, 5}
	rebanada2 := make([]int, 5)
	copy(rebanada2, rebanada1) // destino, origen
	fmt.Println(rebanada1, rebanada2)
}
