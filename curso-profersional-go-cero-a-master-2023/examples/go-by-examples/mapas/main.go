package main

import "fmt"

// https://www.udemy.com/course/curso-golang/learn/lecture/38174430#overview
func main() {
	colors := map[string]string{
		"rojo":  "#FF0000",
		"verde": "#00FF00",
		"azul":  "#0000FF",
	}

	fmt.Println(colors)
	colors["negro"] = "#000000"
	fmt.Println(colors)
	value, ok := colors["rojo1"]
	fmt.Println(value, ok)

	delete(colors, "rojo")
	fmt.Println(colors)
	for clave, valor := range colors {
		fmt.Println(clave, valor)
		fmt.Printf("Clave: %s, Valor: %s\n", clave, valor)
	}
}
