package main

import "fmt"

func divide(dividendo, divisor int) {
	// Captura cualquier panic y al final re recupera del panico
	defer func() { // funcion anónima
		if r := recover(); r != nil {
			fmt.Println(r)
		}
	}()
	validateZero(divisor)
	fmt.Println(dividendo / divisor)

}
func validateZero(divisor int) {
	if divisor == 0 {
		panic("No es posible dividir por cero")
	}

}
func main() {
	divide(9, 3)
	divide(9, 2)
	divide(10, 0) // exit status 2
	divide(9, 1)

}
