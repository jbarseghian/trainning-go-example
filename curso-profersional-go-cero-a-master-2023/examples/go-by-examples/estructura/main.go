package main

import "fmt"

type Persona struct {
	nombre   string
	apellido string
	edad     int
}

func main() {
	// Instancia p como una instancia de Persona
	p := Persona{nombre: "Jime", apellido: "Barse", edad: 10}
	fmt.Println(p)
	fmt.Printf("Persona %+v\n", p)

	var p1 Persona
	fmt.Println(p1)

}
