package main

import (
	"errors"
	"fmt"
	"strconv"
)

func divide(dividendo, divisor int) (int, error) {
	if divisor == 0 {
		//fmt.Errorf("Error")
		return 0, errors.New("No se puede dividir por cero")
	}
	return dividendo / divisor, nil
}
func main() {
	n1, e := divide(10, 2)
	fmt.Println(n1, e)

	n1, e = divide(10, 0)
	if e != nil {
		fmt.Println("Error: ", e)
	} else {
		fmt.Println(n1, e)
	}

	str := "123"
	n, err := strconv.Atoi(str)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("Número:", n)
}
