# Saludos en Go

Este páquete proporciona una forma simple de obtener saludos personalizados en go

## Instalación
Ejecuta el siguiente comando para instalar el paquete:

```nashorn js
go get -u gitlab.com/jbarseghian/greetings
```