package main

import "fmt"

func main() {
	fmt.Println(hello("pepe"))
	fmt.Println(calc(1, 2))
}

func hello(name string) string {
	return "Hola, " + name
}

func calc(a, b int) (sum1, mul1 int) {
	sum := a + b
	mul := a * b
	return sum, mul
}
