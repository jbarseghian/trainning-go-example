package main

import (
	"log"
	"os"
)

func main() {
	file, err := os.OpenFile("info.log", os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	log.SetOutput(file)
	log.SetPrefix("main():")
	log.Print("Soy un log")
	log.Fatal("Oye soy un registro de errores! fatal")
}

/*func main() {
	log.Panic("Oye soy un registro de errores! panic")
	log.Fatal("Oye soy un registro de errores! fatal")
	// No continua el programa
	log.Print("Este es un mensaje de registro")
	log.Println("Este es otro mensaje de registro")
}*/
