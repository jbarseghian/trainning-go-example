package main

import "fmt"

type Persona struct {
	nombre string
	edad   int
	correo string
}

// Receiver go recomenda es pasar
// Con el receptor este es un método que pertenece a esta structura
func (p *Persona) diHola() {
	fmt.Println("Hola, mi nombre es", p.nombre)
}

// Puntero es una variable que almacena la direccion de memoria de otra variable
func main() {
	p1 := Persona{nombre: "Jime", edad: 28, correo: "jime@gmail.com"}
	p1.diHola()

	var x int = 10
	var p *int = &x

	fmt.Println(x, p)
	fmt.Println(&x, p)
	fmt.Println(x, *p)

	fmt.Println(x)
	editar(&x)
	fmt.Println(x)

	fmt.Println(x)
	editar1(x)
	fmt.Println(x)
}

// Modifica el valor de x por tener x
func editar(x *int) {
	*x = 20
}

func editar1(x int) {
	x = 20
}
