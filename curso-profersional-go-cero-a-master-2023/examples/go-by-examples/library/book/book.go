package book

import "fmt"

type Printable interface {
	PrintInfo()
}

func Print(p Printable) {
	p.PrintInfo()
}

type Book struct {
	title  string
	author string
	pages  int
}

// Simular un constructor
func NewBook(title string, author string, pages int) *Book {
	return &Book{title: title, author: author, pages: pages}
}

func (b *Book) SetTitle(title string) {
	b.title = title
}

func (b *Book) GetTitle() string {
	return b.title
}

// Metodo del struct Book
func (b *Book) PrintInfo() {
	fmt.Printf("Title: %s\nAuthor: %s\nPages: %d\n", b.title, b.author, b.pages)
}

type TextBook struct {
	Book
	editorial string
	level     string
}

func NewTextBook(title string, author string, pages int, editorial string, level string) *TextBook {
	return &TextBook{
		Book:      Book{title: title, author: author, pages: pages},
		editorial: editorial,
		level:     level,
	}
}

// Metodo del struct Book
func (t *TextBook) PrintInfo() {
	fmt.Printf("Title: %s\nAuthor: %s\nPages: %d\nEditorial: %s\n Level: %s\n",
		t.title, t.author, t.pages, t.editorial, t.level)
}
