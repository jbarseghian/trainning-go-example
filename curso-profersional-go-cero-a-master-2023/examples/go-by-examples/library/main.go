package main

import (
	"fmt"
	"library/animal"
	"library/book"
)

func main() {
	animales := []animal.Animal{
		&animal.Perro{Nombre: "Agustin"},
		&animal.Gato{Nombre: "Michifus"},
		&animal.Perro{Nombre: "Olaf"},
		&animal.Gato{Nombre: "Luna"},
	}
	// Llamamos método de la interfaz
	for _, a := range animales {
		a.Sonido()
	}
}

func main3() {
	miPerro := animal.Perro{Nombre: "Agustin"}
	miGato := animal.Gato{Nombre: "Michifus"}

	animal.HacerSonido(&miPerro)
	animal.HacerSonido(&miGato)
}

func main2() {
	myBook := book.NewBook("Mody Dick", "Herman Melville", 100)
	myTextBook := book.NewTextBook("Mody Dick", "Herman Melville", 100, "Santillana SAC", "Secundaria")

	//myBook.PrintInfo()
	//myTextBook.PrintInfo()
	// Ambas clases implementan la interface Printable
	book.Print(myBook)
	book.Print(myTextBook)
}

func main1() {
	//myBook := book.Book{Title: "Mody Dick", Author: "Herman Melville", Pages: 100}
	myBook := book.NewBook("Mody Dick", "Herman Melville", 100)
	myBook.PrintInfo()

	myBook.SetTitle("Mody Dick 1")
	fmt.Println(myBook.GetTitle())
	myBook.PrintInfo()

	fmt.Println("========== Text Book ==========")
	myTextBook := book.NewTextBook("Mody Dick", "Herman Melville", 100, "Santillana SAC", "Secundaria")
	myTextBook.PrintInfo()

	myBook1 := book.NewBook("Mody Dick", "Herman Melville", 100)
	myTextBook1 := book.NewTextBook("Mody Dick", "Herman Melville", 100, "Santillana SAC", "Secundaria")

	myBook1.PrintInfo()
	myTextBook1.PrintInfo()
}
