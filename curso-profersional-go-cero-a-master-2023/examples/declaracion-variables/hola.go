package main

import "fmt"

const Pi = 3.14

func main() {
	fmt.Println("Hola Mundo")
	var firstName, lastName string
	var age int

	fmt.Println(firstName, lastName, age)

	firstName = "Alex"
	lastName = "Alex"
	age = 27

	fmt.Println(firstName, lastName, age)
	fmt.Println(Pi)

	fullName := "Alex Roel \t(alias \"roelcode\")\n)"
	fmt.Println(fullName)

	//paquete fmt
	name := "Alex"
	age1 := 28
	fmt.Printf("Hola, me llamo %s y tengo %d.\n", name, age1)

	greeting := fmt.Sprintf("Hola, me llamo %s y tengo %d.\n", name, age1)
	fmt.Println(greeting)
}
