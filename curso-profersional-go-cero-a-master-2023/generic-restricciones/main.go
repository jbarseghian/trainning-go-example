package main

import "fmt"

// Nueva forma de funcion variadica
func PrintList(list ...any) {
	for _, value := range list {
		fmt.Println(value)
	}
}

func PrintListInterface(list ...interface{}) {
	for _, value := range list {
		fmt.Println(value)
	}
}

func Sum(nums ...int) int {
	var total int
	for _, num := range nums {
		total += num
	}
	return total
}

// Parametros de tipos
// [T int] parametro de tipo se le llama restriccion arbitrario
// solo suma de ints
func SumPdT[T int](nums ...T) T {
	var total T
	for _, num := range nums {
		total += num
	}
	return total
}

func SumRestriccionIntFloats[T ~int | ~float64](nums ...T) T {
	var total T
	for _, num := range nums {
		total += num
	}
	return total
}

type integer int

func main() {

	PrintList("Alex", "Roel", "Juan", "Pedro")
	PrintList(100, 456, 789, 456, 452)
	PrintList("HOLA", 452, 4.25, true)

	fmt.Println(Sum(4, 5, 6, 4))
	fmt.Println(SumPdT(4, 5, 6, 4))

	//todos float o todos int
	fmt.Println(SumRestriccionIntFloats(4.5, 5.5, 6.0, 4.0))

	var num1 integer = 100
	var num2 integer = 100
	fmt.Println(SumRestriccionIntFloats(num1, num2))
}
