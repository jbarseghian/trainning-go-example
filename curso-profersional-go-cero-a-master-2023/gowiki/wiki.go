package main

import (
	"errors"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"regexp"
)

type Page struct {
	Title string
	Body  []byte
}

func (p *Page) save() error {
	filename := p.Title + ".txt"
	return os.WriteFile(filename, p.Body, 0600)
}

func loadPage(title string) (*Page, error) {
	filename := title + ".txt"

	body, err := os.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	return &Page{Title: title, Body: body}, nil
}

var validPath = regexp.MustCompile("^/(edit|save|view)/([a-zA-X0-9]+)$")

func getTitle(w http.ResponseWriter, r *http.Request) (string, error) {
	m := validPath.FindStringSubmatch(r.URL.Path)
	if m == nil {
		http.NotFound(w, r)
		return "", errors.New("Título de páina inválido")
	}
	fmt.Println(m)
	return m[2], nil
}

func makeHandler(fn func(http.ResponseWriter, *http.Request, string)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		m := validPath.FindStringSubmatch(r.URL.Path)
		if m == nil {
			http.NotFound(w, r)
			return
		}
		// fmt.Println(m)
		fn(w, r, m[2])
	}
}

// r request para capturar
func viewHandler(w http.ResponseWriter, r *http.Request, title string) {
	//title := r.URL.Path[len("/view/"):]
	p, err := loadPage(title)
	if err != nil {
		http.Redirect(w, r, "/edit/"+title, http.StatusFound)
		return
	}
	renderTemplate(w, "view", p)

	// fmt.Fprintf(w, "<h1>%s</h1> <div>%s</div>", p.Title, p.Body)

	// Mensaje a mostrarle al cliente
	// fmt.Fprintf(w, "¡Hola! Me encantan los %s", "Monos")
	// Del path que capturamos desde la posicion 1
	// http://localhost:8080/monos
	// fmt.Fprintf(w, "¡Hola! Me encantan los %s", r.URL.Path[1:])
}

func editHandler(w http.ResponseWriter, r *http.Request, title string) {
	p, err := loadPage(title)
	if err != nil {
		p = &Page{Title: title}
	}
	renderTemplate(w, "edit", p)
}

func saveHandler(w http.ResponseWriter, r *http.Request, title string) {
	//title := r.URL.Path[len("/save/"):]
	//title, err := getTitle(w, r)
	//if err != nil {
	//	return
	//}
	body := r.FormValue("body")

	p := &Page{Title: title, Body: []byte(body)}
	err := p.save()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	http.Redirect(w, r, "/view/"+title, http.StatusFound)
}

var templates = template.Must(template.ParseFiles("edit.html", "view.html"))

func renderTemplate(w http.ResponseWriter, tmpl string, p *Page) {
	err := templates.ExecuteTemplate(w, tmpl+".html", p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func main() {
	//p1 := &Page{Title: "TestPage", Body: []byte("Esta es una página de muestra")}
	//p1.save()
	//
	//p2, _ := loadPage("TestPage")
	//fmt.Println(p2.Title, string(p2.Body))

	// Recibe una ruta en este caso la principal y una funcion de tipo viewHandler
	// w es para responder al cliente
	// r estructura para obtener la peticion del cliente
	//http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
	//	// Mensaje a mostrarle al cliente
	//	fmt.Fprintf(w, "¡Hola! Me encantan los %s", "Monos")
	//})

	http.HandleFunc("/view/", makeHandler(viewHandler))
	http.HandleFunc("/edit/", makeHandler(editHandler))
	http.HandleFunc("/save/", makeHandler(saveHandler))

	// http://localhost:8080/
	log.Fatal(http.ListenAndServe(":8080", nil))
}
