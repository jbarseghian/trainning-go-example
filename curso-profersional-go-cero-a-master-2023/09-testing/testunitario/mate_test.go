package testunitario

import "testing"

/*
func TestSuma(t *testing.T) {
	total := Suma(5, 5)
	if total != 11 {
		t.Errorf("Suma incorrecta, tiene %d se esperaba %d", total, 10)
	}
}*/

func TestSuma(t *testing.T) {
	tabla := []struct {
		a int
		b int
		c int
	}{
		{1, 2, 3},
		{2, 2, 4},
		{25, 25, 50},
	}

	for _, item := range tabla {
		total := Suma(item.a, item.b)
		if total != item.c {
			t.Errorf("Suma incorrecta, tiene %d se esperaba %d", total, item.c)
		}
	}
}

func TestSumaGetMax(t *testing.T) {
	tabla := []struct {
		a int
		b int
		c int
	}{
		{4, 2, 4},
		{5, 3, 5},
		{1, 3, 3},
	}

	for _, item := range tabla {
		total := GetMax(item.a, item.b)
		if total != item.c {
			t.Errorf("Maximo incorrecto, tiene %d se esperaba %d", total, item.c)
		}
	}
}

func TestFibonacci(t *testing.T) {
	tabla := []struct {
		a int
		b int
	}{
		{1, 1},
		{8, 21},
		{50, 12586269025},
	}

	for _, item := range tabla {
		total := Fibonacci(item.a)
		if total != item.b {
			t.Errorf("Fibonnaci incorrecto, tiene %d se esperaba %d", total, item.b)
		}
	}
}
