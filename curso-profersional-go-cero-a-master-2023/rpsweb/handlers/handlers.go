package handlers

import (
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"rpsweb/rps"
	"strconv"
)

const (
	templateDir  = "templates/"
	templateBase = templateDir + "base.html"
)

type Player struct {
	Name string
}

var player Player

func Index(w http.ResponseWriter, r *http.Request) {
	restartValue()
	renderTemplate(w, "index.html", nil)

	//fmt.Fprintln(w, "Página de inicio")

	// templ := template.Must(template.ParseFiles("templates/base.html", "templates/index.html"))
	//err := templ.ExecuteTemplate(w, "base", nil)
	//if err != nil {
	//	http.Error(w, "Error al renderizar plantillas", http.StatusInternalServerError)
	//	return
	//}

	// Para renderizar con plantilla
	//Lista de nombres de plantillas como argumento
	//templ, err := template.ParseFiles("templates/base.html", "templates/index.html")
	//if err != nil {
	//	http.Error(w, "Error al analizar plantillas", http.StatusInternalServerError)
	//	return
	//}

	//data := struct {
	//	Title   string
	//	Message string
	//}{
	//	Title:   "Página de Inicio",
	//	Message: "¡Bienvenido a piedra, papel o tijera!",
	//}

}

func NewGame(w http.ResponseWriter, r *http.Request) {
	restartValue()
	//fmt.Fprintln(w, "Crear nuevo juego de inicio")
	renderTemplate(w, "new-game.html", nil)
}

func Game(w http.ResponseWriter, r *http.Request) {
	//fmt.Fprintln(w, "Juego")
	if r.Method == "POST" {
		err := r.ParseForm()
		if err != nil {
			http.Error(w, "Error parsing form", http.StatusBadRequest)
			return
		}
		player.Name = r.Form.Get("name")
	}

	fmt.Println(player.Name)
	if player.Name == "" {
		http.Redirect(w, r, "/new", http.StatusFound)
	}

	renderTemplate(w, "game.html", player)
}

func Play(w http.ResponseWriter, r *http.Request) {
	//fmt.Fprintln(w, "Jugar")
	playerChoice, _ := strconv.Atoi(r.URL.Query().Get("c"))
	result := rps.PlayRound(playerChoice)

	out, err := json.MarshalIndent(result, "", "    ")
	if err != nil {
		log.Println(err)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(out)
	fmt.Println(result)
}

func About(w http.ResponseWriter, r *http.Request) {
	//fmt.Fprintln(w, "Acerca de")
	restartValue()
	renderTemplate(w, "about.html", nil)
}

func renderTemplate(w http.ResponseWriter, page string, data any) {
	templ := template.Must(template.ParseFiles(templateBase, templateDir+page))

	err := templ.ExecuteTemplate(w, "base", data)
	if err != nil {
		http.Error(w, "Error al renderizar plantillas", http.StatusInternalServerError)
		log.Println(err)
		return
	}
}

// Reiniciar valores

func restartValue() {
	player.Name = ""
	rps.ComputerScore = 0
	rps.PlayerScore = 0
}
