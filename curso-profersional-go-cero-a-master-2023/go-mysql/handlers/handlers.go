package handlers

import (
	"database/sql"
	"fmt"
	"go-mysql/models"
	"log"
)

func ListContacts(db *sql.DB) {
	query := "select * from contact"

	rows, err := db.Query(query)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	fmt.Println("\nLISTA DE CONTACTOS:")
	fmt.Println("-------------------------------------------------------------------------------------")

	for rows.Next() {
		contact := models.Contact{}
		//Scaneo y lo guardo en contact
		var valueEmail sql.NullString
		err := rows.Scan(&contact.Id, &contact.Name, &valueEmail, &contact.Phone)
		if err != nil {
			log.Fatal(err)
		}

		if valueEmail.Valid {
			contact.Email = valueEmail.String
		} else {
			contact.Email = "Sin correo electrónico"
		}

		fmt.Printf("ID: %d, Nombre: %s, Email: %s, Teléfono: %s\n",
			contact.Id,
			contact.Name,
			contact.Email,
			contact.Phone)
		fmt.Println("-------------------------------------------------------------------------------------")
	}
}

func GetContactById(db *sql.DB, contactId int) {
	query := "select * from contact where id = ?"

	row := db.QueryRow(query, contactId)

	contact := models.Contact{}
	var valueEmail sql.NullString
	err := row.Scan(&contact.Id, &contact.Name, &valueEmail, &contact.Phone)
	if err != nil {
		if err == sql.ErrNoRows {
			log.Fatalf("no se encontró ningún contacto con el ID %d", contactId)
		}
	}

	if valueEmail.Valid {
		contact.Email = valueEmail.String
	} else {
		contact.Email = "Sin correo electrónico"
	}
	fmt.Println("\nLISTA DE UN CONTACTO:")
	fmt.Println("-------------------------------------------------------------------------------------")
	fmt.Printf("ID: %d, Nombre: %s, Email: %s, Teléfono: %s\n",
		contact.Id,
		contact.Name,
		contact.Email,
		contact.Phone)
	fmt.Println("-------------------------------------------------------------------------------------")
}

func CreateContact(db *sql.DB, contact models.Contact) {
	query := "INSERT INTO contact (name, email, phone) VALUES(?,?,?)"

	_, err := db.Exec(query, contact.Name, contact.Email, contact.Phone)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Nuevo contacto registrado con éxito")
}

func UpdateContact(db *sql.DB, contact models.Contact) {
	query := "update contact set name = ?, email = ?, phone = ? where id = ?"

	_, err := db.Exec(query, contact.Name, contact.Email, contact.Phone, contact.Id)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Contacto actualizado con éxito")
}

func DeleteContact(db *sql.DB, contactId int) {
	query := "delete from contact where  id = ?"

	_, err := db.Exec(query, contactId)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Contacto elimindao con éxito")
}
